import geb.spock.GebSpec


class SampleGebSpec extends GebSpec {
    def 'googleで「groovy web テスト」を検索'() {

        when:
        go()
        waitFor{ $('input', name: 'q') } << 'groovy web テスト'
        waitFor{ $('[name="btnK"]') }.click()

        then:
        waitFor(10) { page.text() =~ 'Geb' }

    }
}