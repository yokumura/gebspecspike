class HalloSpec extends spock.lang.Specification {
    def 'はじめてのSpec'() {
        expect: x + y == z
        where:
            x | y | z
            3 | 4 | 7
           12 |34 |46
    }

    def '日本語の比較'() {
        expect: a + i == u
        where:
              a | i | u
            'あ'|'い'|'あい'
    }
}
